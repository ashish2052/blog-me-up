const express = require("express");
const app = express();
const config = require("./config/index");
const apiRoute = require("./controllers/api.route")();
const morgan = require('morgan');
const path = require('path');
require("./db")

const bodyParser = require('body-parser')

const cors = require("cors");

app.use


// Third party middlewares
app.use(morgan('dev'));

app.use('/image', express.static(path.join(__dirname, 'uploads')))
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

app.use(cors());



// Routes

app.use("/api", apiRoute);



// error handling Middleware
app.use(function (err, req, res, next) {
    res.status(err.status || 400)
    res.json({
        errmsg: err.msg || err,
        status: err.status || 400,
    })
})







app.listen(config.port, (err, done) => {
    if (err) {
        console.log("Problem loading server")
    }
    else {
        console.log(`Server listening at port ${config.port}`)
    }
})