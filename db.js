const mongoose = require("mongoose");
const { conxnURL, dbName } = require("./config/db.config");

mongoose.connect(`${conxnURL}/${dbName}`, { useNewUrlParser: true },
    (err, done) => {
        if (err) {
            console.log("Database connecttion failed");
        } else {
            console.log("Database connection successfull")
        }
    })